import {
    VuexModule,
    Module,
    MutationAction,
    Mutation,
    Action,
    getModule
} from 'vuex-module-decorators'
import {
    login,
    register,
    getInfo
} from '@/api/login'
import {
    getToken,
    setToken,
    removeToken
} from '@/utils/auth'
import store from '@/store'

export interface IUserState {
    token: string
    name: string
    avatar: string
    color: string
    roles: string[]
    menus: string[]
    releaseStamp: string
}

@Module({
    dynamic: true,
    store,
    name: 'user'
})
class User extends VuexModule implements IUserState {
    token = ''
    name = ''
    avatar = ''
    color = ''
    email = ''
    releaseStamp = ''
    roles = []
    menus = []

    @Mutation
    SET_TOKEN(token: string) {
        this.token = token
    }

    @Action({
        commit: 'SET_TOKEN'
    })
    async Login(userInfo: {
        username: string,
        password: string
    }) {
        const username = userInfo.username.trim()
        const data = await login(username, userInfo.password)
        const token = data && data.login && data.login.token
        if (token) {
            setToken(token)
            return token
        } else {
            throw new Error('login error')
        }
    }

    @Action({
        commit: 'SET_TOKEN'
    })
    async Register(userInfo: {
        username: string,
        email: string,
        password: string,
        rePassword: string
    }) {
        // const username = userInfo.username.trim()
        const data = await register(userInfo.username, userInfo.password, userInfo.email)
        const token = data.data.token
        setToken(token)
        return token
    }

    @Action({
        commit: 'SET_TOKEN'
    })
    async FedLogOut() {
        removeToken()
        return ''
    }

    @MutationAction({
        mutate: ['token', 'roles', 'name', 'email', 'avatar', 'color', 'menus', 'releaseStamp']
    })
    async GetInfo() {
        const token = getToken()
        const menus: any[] = []
        const localReleaseStamp = window.localStorage.getItem('releaseStamp')
        const configUser = window.localStorage.getItem('configUser')

        if (token === undefined) {
            throw Error('GetInfo: token is undefined!')
        }
        const {roles, user = {}, menu, releaseStamp}: {roles: any, user: any, menu: any, releaseStamp: string} = await getInfo(token) || {}
        console.log('GetInfo res', roles, user, menu)

        // if (roles && !roles.length) {
        //     throw Error('GetInfo: roles must be a non-null array!')
        // }

        if ((releaseStamp && localReleaseStamp !== releaseStamp) || (user.id && user.id !== configUser)) { // 版本不同就清空
            window.localStorage.setItem('modules', '')
            window.localStorage.setItem('releaseStamp', releaseStamp)
            window.localStorage.setItem('configUser', user.id)
        }

        interface Menu {
            path: string,
            name: string,
            icon: string,
            pageName: string,
            children: Menu[]
        }
        interface PageMenu {
            path: string,
            pageName?: string,
            meta: {
                title: string,
                icon: string
            },
            children?: PageMenu[]
        }

        // 把menu转化成前端的配置
        const traverse = (arr: Menu[]): PageMenu[] => {
            let menuTmp = []
            menuTmp = arr && arr.length && arr.map(({children, path, icon, name, pageName}: Menu) => {
                const childrenTmp: PageMenu = {
                    path,
                    pageName,
                    meta: {
                        title: name,
                        icon
                    }
                }
                if (children && children.length) {
                    childrenTmp.children = traverse(children)
                } else { // 单节点菜单也需要children
                    childrenTmp.children = [{
                        path,
                        pageName,
                        meta: {
                            title: name,
                            icon
                        }
                    }]
                }
                return childrenTmp
            }) || []

            return menuTmp
        }

        console.log('traverse(menu): ', traverse(menu))
        return {
            token,
            roles,
            name: user && user.username,
            email: user && user.email,
            avatar: user && user.avatar,
            color: user && user.color,
            menus: traverse(menu),
            releaseStamp
        }
    }

    @MutationAction({
        mutate: ['token', 'roles']
    })
    async LogOut() {
        if (getToken() === undefined) {
            throw Error('LogOut: token is undefined!')
        }
        // await logout()
        removeToken()
        return {
            token: '',
            roles: []
        }
    }
}

export const UserModule = getModule(User)
