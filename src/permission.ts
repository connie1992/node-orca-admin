import router from './router'
import NProgress from 'nprogress'
import {Message} from 'element-ui'
import {getToken} from '@/utils/auth'
import {Route} from 'vue-router'
import {UserModule} from '@/store/modules/user'

import 'nprogress/nprogress.css'

const whiteList = ['/login']

// 路由变化时的钩子
router.beforeEach((to: Route, from: Route, next: any) => {
    NProgress.start()
    if (getToken()) {
        if (to.path === '/login') {
            next({
                path: '/'
            })
            NProgress.done() // If current page is dashboard will not trigger afterEach hook, so manually handle it
        } else {
            if (!UserModule.roles || UserModule.roles.length === 0) {
                UserModule.GetInfo().then(() => {
                    next()
                }).catch(err => {
                    UserModule.FedLogOut().then(() => {
                        Message.error(err || 'Verification failed, please login again')
                        next({
                            path: '/'
                        })
                    })
                })
            } else {
                next()
            }
        }
    } else {
        if (whiteList.indexOf(to.path) !== -1) {
            next()
        } else {
            next(`/login?redirect=${to.path}`) // 否则全部重定向到登录页
            NProgress.done()
        }
    }
})

router.afterEach(() => {
    NProgress.done()
})
