export function formatTime(stm, reg, flag=true) {
  stm = stm && new Date(stm) || new Date()
  reg = reg || '-- ::'
  let res
  if (!flag) {
    res = stm.getFullYear() + reg[0] + (stm.getMonth() + 1) 
      + reg[1] + stm.getDate()
  } else {
    res = stm.getFullYear() + reg[0] + (stm.getMonth() + 1) 
      + reg[1] + stm.getDate() + reg[2] + stm.getHours() + reg[3]
      +stm.getMinutes() + reg[4] + stm.getSeconds()
  }
  return res
}