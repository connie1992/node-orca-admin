import axios from 'axios'
import { Message, MessageBox } from 'element-ui'
import { getToken } from '@/utils/auth'
import { UserModule } from '@/store/modules/user'

// 创建axios实例
const service = axios.create({
    baseURL: 'http://localhost:7001',
    timeout: 5000
})

// request 拦截器
service.interceptors.request.use(
    config => {
        if (UserModule.token) {
            config.headers['User-Token'] = getToken() // 让每个请求携带自定义token 请根据实际情况自行修改
        }
        return config
    },
    error => {
        // Handle request error here
        Promise.reject(error)
    }
)

// respone 拦截器
service.interceptors.response.use(
    response => {
        /**
         * code为非20000是抛错 可结合自己业务进行修改
         */
        const res = response.data
        const {success, code} = res
        if (!success && !code && +code !== 100) {
        console.log(res, 'error~~~')
        Message({
                message: res.message || res.data.err_msg,
                type: 'error',
                duration: 5 * 1000
            })

        return Promise.reject('error ')
        } else {
            return response.data
        }
    },
    error => {
        Message({
            message: error.message,
            type: 'error',
            duration: 5 * 1000
        })
        return Promise.reject(error)
    }
)

export default service
