import { GraphQLClient } from 'graphql-request'
import { Message, MessageBox } from 'element-ui'
import { getToken } from '@/utils/auth'
import { UserModule } from '@/store/modules/user'

// 实例化
const headers: any = {}

// const client = new GraphQLClient(window.location.origin + '/graphql?', {headers})
const client = new GraphQLClient('http://127.0.0.1:7001/graphql?', {headers})

type G<T extends any> = (query: string, variables?: any) => Promise<T>

export const graphql: G<any> = (query: string, variables?: any) => {
    client.setHeader('User-Token', getToken() || '')
    return client.request(query, variables).catch((error: any) => {
        const defaultErr = 'An unexpected error occurred. Please try again later...'

        if (error && error.message) {
            try {
                const result = JSON.parse('{' + error.message.match(/\"response\"\:[\s\S]*/)[0])
                const errorArr = result.response.errors
                const status: number = result.response.status
                const {message, extensions} = errorArr && errorArr[0] || {message: null, extensions: null}
                console.log('message, extensions: ', message, extensions)

                if (parseInt((status / 100).toString(), 10) === 5) {
                    Message({
                        message: 'response status:' + status,
                        type: 'error',
                        duration: 5 * 1000
                    })
                } else if (message) {
                    try {
                        const {code, msg} = JSON.parse(message)

                        // U001: Your username and password don\'t match!
                        // U002: use is blocked!
                        // U003: token is invaild...
                        if (code && code === 'U003') {
                            MessageBox.confirm(
                                '你已被登出，可以取消继续留在该页面，或者重新登录',
                                '确定登出', {
                                    confirmButtonText: '重新登录',
                                    cancelButtonText: '取消',
                                    type: 'warning'
                                }
                            ).then(() => {
                                UserModule.FedLogOut().then(() => {
                                    location.reload() // 为了重新实例化vue-router对象 避免bug
                                })
                            })
                        } else {
                            Message({
                                message: msg || defaultErr,
                                type: 'error',
                                duration: 5 * 1000
                            })
                        }
                    } catch (e) {
                        Message({
                            message,
                            type: 'error',
                            duration: 5 * 1000
                        })
                    }
                } else if (extensions && extensions.code) {
                    Message({
                        message: extensions.code,
                        type: 'error',
                        duration: 5 * 1000
                    })
                } else {
                    Message({
                        message: defaultErr,
                        type: 'error',
                        duration: 5 * 1000
                    })
                }
            } catch (e) {
                console.log('e: ', e)
                Message({
                    message: e.toString(),
                    type: 'error',
                    duration: 5 * 1000
                })
            }
        }
    })
}
