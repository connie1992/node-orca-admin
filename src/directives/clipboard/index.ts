import Clipboard from 'clipboard'
import { DirectiveOptions } from 'vue'

if (!Clipboard) throw new Error('you shuold npm install `clipboard` --save at frist')

let successCallback: ((e: any) => void) | null
let errorCallback: ((e: any) => void) | null
let clipboardInstance: Clipboard | null

export const clipboard: DirectiveOptions = {
    bind(el, binding) {
        if (binding.arg === 'success') {
            successCallback = binding.value
        } else if (binding.arg === 'error') {
            errorCallback = binding.value
        } else {
            clipboardInstance = new Clipboard(el, {
                text() { return binding.value },
                action() { return binding.arg === 'cut' ? 'cut' : 'copy' }
            })
            clipboardInstance.on('success', e => {
                const callback = successCallback
                if (callback) callback(e)
            })
            clipboardInstance.on('error', e => {
                const callback = errorCallback
                if (callback) callback(e)
            })
        }
    },

    update(el, binding) {
        if (binding.arg === 'success') {
            successCallback = binding.value
        } else if (binding.arg === 'error') {
            errorCallback = binding.value
        } else {
            clipboardInstance = new Clipboard(el, {
                text() { return binding.value },
                action() { return binding.arg === 'cut' ? 'cut' : 'copy' }
            })
        }
    },

    unbind(_, binding) {
        if (binding.arg === 'success') {
            successCallback = null
        } else if (binding.arg === 'error') {
            errorCallback = null
        } else {
            if (clipboardInstance) {
                clipboardInstance.destroy()
            }
            clipboardInstance = null
        }
    }
}

