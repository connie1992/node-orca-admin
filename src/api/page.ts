import {
    graphql
} from '@/utils/graphql'

export async function getList() {
    // const query = /* GraphQL */ `
    // query getConfig($modulesName: String) {
    //     config(modulesName: $modulesName) {
    //         base {
    //             name
    //             display_name
    //             path
    //         },
    //         fields {
    //             name
    //             type
    //             show_type
    //             width
    //         }
    //     }
    // }
    // `
    // const variables = {
    //     modulesName
    // }

    // return await graphql(query, variables)
    return {
        total: 7,
        data: [{
                id: 3382,
                created_at: '2019-02-20T08:08:14.000Z',
                updated_at: '2019-02-20T08:08:14.000Z',
                notify: 0,
                type: 0,
                own_id: 55,
                own_name: 'fanmuchen',
                title: '【PST 2月18日-19日部分订单可发货】',
                content: '<p style=\'margin-top: 20px; margin-bottom: 20px; color: rgb(34, 34, 34);\'><span style=\'font-size: 20px; color: rgb(34, 34, 34); font-family: PingFangSC, &quot;Open Sans&quot;, sans-serif;\'>商家你好！</span></p><p style=\'margin-top: 1em; margin-bottom: 1em; color: rgb(34, 34, 34); font-size: 15px;\'><span style=\'font-size: 20px; color: rgb(0, 0, 0); font-family: &quot;lucida Grande&quot;, Verdana;\'>PST时间 2月18日-19日的部分订单可以发货了！</span></p><p style=\'margin-top: 20px; margin-bottom: 20px; font-size: 15px; color: rgb(0, 0, 0); font-family: PingFangSC, &quot;Open Sans&quot;, sans-serif;\'></p><p style=\'margin-top: 20px; margin-bottom: 20px; color: rgb(34, 34, 34);\'><br></p><p style=\'margin-top: 1em; margin-bottom: 1em; color: rgb(34, 34, 34); font-size: 15px;\'><span style=\'font-size: 20px; color: rgb(0, 0, 0); font-family: &quot;lucida Grande&quot;, Verdana;\'>具体时间范围： PST 2月18日 20：00 ~ 2月19日 19：59</span></p><p style=\'margin-top: 20px; margin-bottom: 20px; font-size: 15px; color: rgb(0, 0, 0); font-family: PingFangSC, &quot;Open Sans&quot;, sans-serif;\'></p><p style=\'margin-top: 20px; margin-bottom: 20px; color: rgb(34, 34, 34);\'><br></p><p style=\'margin-top: 20px; margin-bottom: 20px; color: rgb(34, 34, 34);\'><span style=\'font-size: 20px; color: rgb(0, 0, 0); font-family: &quot;lucida Grande&quot;, Verdana;\'>请及时发货，上传物流信息，以免影响结算，谢谢！</span></p>',
                remark: ''
            },
            {
                id: 3383,
                created_at: '2019-02-20T08:08:14.000Z',
                updated_at: '2019-02-20T08:08:14.000Z',
                notify: 0,
                type: 0,
                own_id: 55,
                own_name: 'fanmuchen',
                title: '【PST 2月18日-19日部分订单可发货】',
                content: '<p style=\'margin-top: 20px; margin-bottom: 20px; color: rgb(34, 34, 34);\'><span style=\'font-size: 20px; color: rgb(34, 34, 34); font-family: PingFangSC, &quot;Open Sans&quot;, sans-serif;\'>商家你好！</span></p><p style=\'margin-top: 1em; margin-bottom: 1em; color: rgb(34, 34, 34); font-size: 15px;\'><span style=\'font-size: 20px; color: rgb(0, 0, 0); font-family: &quot;lucida Grande&quot;, Verdana;\'>PST时间 2月18日-19日的部分订单可以发货了！</span></p><p style=\'margin-top: 20px; margin-bottom: 20px; font-size: 15px; color: rgb(0, 0, 0); font-family: PingFangSC, &quot;Open Sans&quot;, sans-serif;\'></p><p style=\'margin-top: 20px; margin-bottom: 20px; color: rgb(34, 34, 34);\'><br></p><p style=\'margin-top: 1em; margin-bottom: 1em; color: rgb(34, 34, 34); font-size: 15px;\'><span style=\'font-size: 20px; color: rgb(0, 0, 0); font-family: &quot;lucida Grande&quot;, Verdana;\'>具体时间范围： PST 2月18日 20：00 ~ 2月19日 19：59</span></p><p style=\'margin-top: 20px; margin-bottom: 20px; font-size: 15px; color: rgb(0, 0, 0); font-family: PingFangSC, &quot;Open Sans&quot;, sans-serif;\'></p><p style=\'margin-top: 20px; margin-bottom: 20px; color: rgb(34, 34, 34);\'><br></p><p style=\'margin-top: 20px; margin-bottom: 20px; color: rgb(34, 34, 34);\'><span style=\'font-size: 20px; color: rgb(0, 0, 0); font-family: &quot;lucida Grande&quot;, Verdana;\'>请及时发货，上传物流信息，以免影响结算，谢谢！</span></p>',
                remark: ''
            },
            {
                id: 3384,
                created_at: '2019-02-20T08:08:14.000Z',
                updated_at: '2019-02-20T08:08:14.000Z',
                notify: 0,
                type: 0,
                own_id: 55,
                own_name: 'fanmuchen',
                title: '【PST 2月18日-19日部分订单可发货】',
                content: '<p style=\'margin-top: 20px; margin-bottom: 20px; color: rgb(34, 34, 34);\'><span style=\'font-size: 20px; color: rgb(34, 34, 34); font-family: PingFangSC, &quot;Open Sans&quot;, sans-serif;\'>商家你好！</span></p><p style=\'margin-top: 1em; margin-bottom: 1em; color: rgb(34, 34, 34); font-size: 15px;\'><span style=\'font-size: 20px; color: rgb(0, 0, 0); font-family: &quot;lucida Grande&quot;, Verdana;\'>PST时间 2月18日-19日的部分订单可以发货了！</span></p><p style=\'margin-top: 20px; margin-bottom: 20px; font-size: 15px; color: rgb(0, 0, 0); font-family: PingFangSC, &quot;Open Sans&quot;, sans-serif;\'></p><p style=\'margin-top: 20px; margin-bottom: 20px; color: rgb(34, 34, 34);\'><br></p><p style=\'margin-top: 1em; margin-bottom: 1em; color: rgb(34, 34, 34); font-size: 15px;\'><span style=\'font-size: 20px; color: rgb(0, 0, 0); font-family: &quot;lucida Grande&quot;, Verdana;\'>具体时间范围： PST 2月18日 20：00 ~ 2月19日 19：59</span></p><p style=\'margin-top: 20px; margin-bottom: 20px; font-size: 15px; color: rgb(0, 0, 0); font-family: PingFangSC, &quot;Open Sans&quot;, sans-serif;\'></p><p style=\'margin-top: 20px; margin-bottom: 20px; color: rgb(34, 34, 34);\'><br></p><p style=\'margin-top: 20px; margin-bottom: 20px; color: rgb(34, 34, 34);\'><span style=\'font-size: 20px; color: rgb(0, 0, 0); font-family: &quot;lucida Grande&quot;, Verdana;\'>请及时发货，上传物流信息，以免影响结算，谢谢！</span></p>',
                remark: ''
            },
            {
                id: 3385,
                created_at: '2019-02-20T08:08:14.000Z',
                updated_at: '2019-02-20T08:08:14.000Z',
                notify: 0,
                type: 0,
                own_id: 55,
                own_name: 'fanmuchen',
                title: '【PST 2月18日-19日部分订单可发货】',
                content: '<p style=\'margin-top: 20px; margin-bottom: 20px; color: rgb(34, 34, 34);\'><span style=\'font-size: 20px; color: rgb(34, 34, 34); font-family: PingFangSC, &quot;Open Sans&quot;, sans-serif;\'>商家你好！</span></p><p style=\'margin-top: 1em; margin-bottom: 1em; color: rgb(34, 34, 34); font-size: 15px;\'><span style=\'font-size: 20px; color: rgb(0, 0, 0); font-family: &quot;lucida Grande&quot;, Verdana;\'>PST时间 2月18日-19日的部分订单可以发货了！</span></p><p style=\'margin-top: 20px; margin-bottom: 20px; font-size: 15px; color: rgb(0, 0, 0); font-family: PingFangSC, &quot;Open Sans&quot;, sans-serif;\'></p><p style=\'margin-top: 20px; margin-bottom: 20px; color: rgb(34, 34, 34);\'><br></p><p style=\'margin-top: 1em; margin-bottom: 1em; color: rgb(34, 34, 34); font-size: 15px;\'><span style=\'font-size: 20px; color: rgb(0, 0, 0); font-family: &quot;lucida Grande&quot;, Verdana;\'>具体时间范围： PST 2月18日 20：00 ~ 2月19日 19：59</span></p><p style=\'margin-top: 20px; margin-bottom: 20px; font-size: 15px; color: rgb(0, 0, 0); font-family: PingFangSC, &quot;Open Sans&quot;, sans-serif;\'></p><p style=\'margin-top: 20px; margin-bottom: 20px; color: rgb(34, 34, 34);\'><br></p><p style=\'margin-top: 20px; margin-bottom: 20px; color: rgb(34, 34, 34);\'><span style=\'font-size: 20px; color: rgb(0, 0, 0); font-family: &quot;lucida Grande&quot;, Verdana;\'>请及时发货，上传物流信息，以免影响结算，谢谢！</span></p>',
                remark: ''
            },
            {
                id: 3386,
                created_at: '2019-02-20T08:08:14.000Z',
                updated_at: '2019-02-20T08:08:14.000Z',
                notify: 0,
                type: 0,
                own_id: 55,
                own_name: 'fanmuchen',
                title: '【PST 2月18日-19日部分订单可发货】',
                content: '<p style=\'margin-top: 20px; margin-bottom: 20px; color: rgb(34, 34, 34);\'><span style=\'font-size: 20px; color: rgb(34, 34, 34); font-family: PingFangSC, &quot;Open Sans&quot;, sans-serif;\'>商家你好！</span></p><p style=\'margin-top: 1em; margin-bottom: 1em; color: rgb(34, 34, 34); font-size: 15px;\'><span style=\'font-size: 20px; color: rgb(0, 0, 0); font-family: &quot;lucida Grande&quot;, Verdana;\'>PST时间 2月18日-19日的部分订单可以发货了！</span></p><p style=\'margin-top: 20px; margin-bottom: 20px; font-size: 15px; color: rgb(0, 0, 0); font-family: PingFangSC, &quot;Open Sans&quot;, sans-serif;\'></p><p style=\'margin-top: 20px; margin-bottom: 20px; color: rgb(34, 34, 34);\'><br></p><p style=\'margin-top: 1em; margin-bottom: 1em; color: rgb(34, 34, 34); font-size: 15px;\'><span style=\'font-size: 20px; color: rgb(0, 0, 0); font-family: &quot;lucida Grande&quot;, Verdana;\'>具体时间范围： PST 2月18日 20：00 ~ 2月19日 19：59</span></p><p style=\'margin-top: 20px; margin-bottom: 20px; font-size: 15px; color: rgb(0, 0, 0); font-family: PingFangSC, &quot;Open Sans&quot;, sans-serif;\'></p><p style=\'margin-top: 20px; margin-bottom: 20px; color: rgb(34, 34, 34);\'><br></p><p style=\'margin-top: 20px; margin-bottom: 20px; color: rgb(34, 34, 34);\'><span style=\'font-size: 20px; color: rgb(0, 0, 0); font-family: &quot;lucida Grande&quot;, Verdana;\'>请及时发货，上传物流信息，以免影响结算，谢谢！</span></p>',
                remark: ''
            }
        ]
    }
}

export async function getDetail(id: number) {
    console.log('id: ', id)
    return {
        data: {
            id: 3609,
            created_at: '2019-02-22T02:47:46.000Z',
            updated_at: '2019-02-22T02:47:46.000Z',
            notify: 0,
            type: 0,
            own_id: 55,
            own_name: 'fanmuchen',
            title: '【PST 2月19日-20日部分订单可发货】',
            content: '<p style=\"margin-top: 20px; margin-bottom: 20px; color: rgb(34, 34, 34);\"><span style=\"font-size: 20px; color: rgb(34, 34, 34); font-family: PingFangSC, &quot;Open Sans&quot;, sans-serif;\">商家你好！</span></p><p style=\"margin-top: 1em; margin-bottom: 1em; color: rgb(34, 34, 34); font-size: 15px;\"><span style=\"font-size: 20px; color: rgb(0, 0, 0); font-family: &quot;lucida Grande&quot;, Verdana;\">PST时间 2月19日-20日的部分订单可以发货了！</span></p><p style=\"margin-top: 20px; margin-bottom: 20px; font-size: 15px; color: rgb(0, 0, 0); font-family: PingFangSC, &quot;Open Sans&quot;, sans-serif;\"></p><p style=\"margin-top: 20px; margin-bottom: 20px; color: rgb(34, 34, 34);\"><br></p><p style=\"margin-top: 1em; margin-bottom: 1em; color: rgb(34, 34, 34); font-size: 15px;\"><span style=\"font-size: 20px; color: rgb(0, 0, 0); font-family: &quot;lucida Grande&quot;, Verdana;\">具体时间范围： PST 2月19日 20：00 ~ 2月20日 19：59</span></p><p style=\"margin-top: 20px; margin-bottom: 20px; font-size: 15px; color: rgb(0, 0, 0); font-family: PingFangSC, &quot;Open Sans&quot;, sans-serif;\"></p><p style=\"margin-top: 20px; margin-bottom: 20px; color: rgb(34, 34, 34);\"><br></p><p style=\"margin-top: 20px; margin-bottom: 20px; color: rgb(34, 34, 34);\"><span style=\"font-size: 20px; color: rgb(0, 0, 0); font-family: &quot;lucida Grande&quot;, Verdana;\">请及时发货，上传物流信息，以免影响结算，谢谢！</span></p>',
            remark: ''
        }
    }
}
