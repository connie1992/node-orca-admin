import {graphql} from '@/utils/graphql'

export async function getConfig(modulesName: string) {
    const query = /* GraphQL */ `
    query getConfig($modulesName: String) {
        config(modulesName: $modulesName) {
            base {
                name
                display_name
                path
            },
            pages {
                list {
                    text_search
                    adv_search
                    select_search
                    listIndex
                    selection
                    top_buttons {
                        name
                        title
                        type
                    }
                    bottom_buttons {
                        name
                        title
                        type
                    }
                    layouts {
                        fields
                        orderby {
                            field
                            sort
                        }
                    }
                }
                detail {
                    top_buttons {
                        name
                        title
                        type
                    }
                    layouts {
                        summary
                        groups {
                            title
                            tabs {
                                id
                                title
                                name
                                fields
                            }
                        }
                    }
                }
            }
            fields {
                name
                display_name
                type
                show_type
                width
                is_primary
                module {
                    name
                }
            }
        }
    }
    `
    const variables = {
        modulesName
    }

    return await graphql(query, variables)
}
