import { UserModule } from './../store/modules/user'
import request from '@/utils/request'
import {graphql} from '@/utils/graphql'

export async function login(username: string, password: string) {
    const query = /* GraphQL */ `
    mutation login($username: String!,  $password: String!) {
        login(username: $username, password: $password) {
            token
        }
    }
    `
    const variables = {
        username,
        password
    }
    return await graphql(query, variables)
}

export async function register(username: string, password: string, email: string) {
    return request({
        url: '/user/register',
        method: 'post',
        data: {
            username, password, email
        }
    })
}

export async function getInfo(token: string) {
    const query = /* GraphQL */ `
    query getUser {
        user {
            id
            username
            email
            avatar
            color
        }
        roles {
            name
            id
        }
        menu {
            name
            icon
            path
            pageName
            children {
                path
                name
                icon
                pageName
                children {
                    path
                    name
                    icon
                    pageName
                    children {
                        path
                        name
                        icon
                        pageName
                        children {
                            path
                            name
                            icon
                            pageName
                            children {
                                path
                                name
                                icon
                                pageName
                            }
                        }
                    }
                }
            }
        }
        releaseStamp
    }
    `

    return await graphql(query)
}

export function logout() {
    return request({
        url: '/user/logout',
        method: 'post'
    })
}
