// import request from '@/utils/request'
import {graphql} from '@/utils/graphql'


export async function getList(data: any) {
    const query = /* GraphQL */ `
    query userList($params: PageQuery) {
        userList(params: $params) {
            total,
            list {
                id,
                username,
                email,
                state,
                createdAt,
                loginAt
            }
        }
    }
    `
    return await graphql(query, { params: data })
}

export async function updateAdd(data: any) {
    // return request({
    //     url: '/user/updateAdd',
    //     method: 'post',
    //     data
    // })
    const query = /* GraphQL */ `
    mutation userUpdateAdd($params: UserParams) {
        userUpdateAdd(params: $params) {
            id,
            username,
            message
        }
    }
    `
    return await graphql(query, { params: data })
}

export async function del(id: string) {
    // return request({
    //     url: '/user/del',
    //     method: 'post',
    //     data
    // })
    const query = /* GraphQL */ `
    mutation userDel($id: String!) {
        userDel(id: $id) {
            id
        }
    }
    `
    return await graphql(query, { id })
}
