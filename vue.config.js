module.exports = {
    publicPath: process.env.NODE_ENV === 'production' ? '/page' : '/',
    outputDir: 'public/dist',
    transpileDependencies: ['vuex-module-decorators'],
    pwa: {
        name: 'orca'
    },
    devServer: {
        disableHostCheck: true,
        // proxy: { // 不能直接代理/，要写具体的接口这样才会触发vue-router history而不会走代理，不然匹配不到资源会代理
            // '/graphql': {target: 'http://127.0.0.1:7001'},
            // '/api': {target: 'http://127.0.0.1:7001'}
        // }
    }
}
